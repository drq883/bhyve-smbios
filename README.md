# bhyve-smbios

This has been tested on FreeBSD 13-STABLE

This repo contains a patch file and utilities to patch bhyve to allow
for the smbios type 11 (oemstrings) with the bhybe -o option

The Makefile has 5 targets:

- patch   - patch and build bhyve (vmrun.sh is also patched)
- revert  - revert patched source to original
- create  - create a bhyve.patch-TEST file from current changes
- build   - rebuild bhyve from current sources
- install - install bhyve from current sources

# To create a new patch file after more modifications

- make create
- mv bhyve.patch-TEST bhyve.patch

# To use oemstrings with bhyve

After patching and building bhyve (make patch), just add options, such as the following, to the bhyve command line:
```
-o oemstring.1=somevalue
-o oemstring.3=someothervalue
```
Then in the resulting VM, use the `dmidecode -t11` to see them
```
# dmidecode -t11
# dmidecode 3.4
# SMBIOS entry point at 0xbfbcb000
Found SMBIOS entry point in EFI, reading table from /dev/mem.
SMBIOS 2.8 present.

Handle 0x0006, DMI type 11, 5 bytes
OEM Strings
        String 1: 'somevalue'
        String 2: N/A
        String 3: 'someothervalue'
```

**NOTE:**  Any string sequence number that is missing is set to N/A as String 2 above.
